const mongoose = require('../database/indexDataBase');

const BookSchema = new mongoose.Schema({

   nameBook: {
   	   type: String,
	   require: true,
	   uppercase: true,
   },

   authorBook: {
       type: String,
	   require: true,
	   uppercase: true,
   },

   createdAt: {
   	   type: Date,
	   default: Date.now,
   },

});

const Book = mongoose.model('Book', BookSchema);

module.exports = Book;