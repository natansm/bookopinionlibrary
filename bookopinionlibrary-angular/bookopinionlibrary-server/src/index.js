const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');

const app = express();

app.use(function(req, res, next) {
      res.header("Access-Control-Allow-Origin", "*");
	  res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
      res.header("Access-Control-Allow-Headers", "Content-Type");
      next();
    });

app.use(express.static('../../dist/bookopinionlibrary-angular'));

app.listen(process.env.PORT || 3000);

app.get('/', (req,res) => {
    res.sendFile(path.join('../../dist/bookopinionlibrary-angular/index.html'));
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded( { extended: false } ));

require('./controller/bookController')(app);
