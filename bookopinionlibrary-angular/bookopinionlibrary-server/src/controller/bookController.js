const express = require('express');

const Book = require('../models/book');

const router = express.Router();

router.get('/get', async(req, res) => {
    res.send(await Book.find(null));
});

router.get('/get/:id', async(req, res) => {
    res.send(await Book.findById(req.params.id));
});

router.post('/add', async (req, res) => {
    
	try{
		const bookNew = await Book.create(req.body);
		return res.send({ bookNew });
	} catch (err) {
		return res.status(400).send({ error: "Error create book" });
	}

});

router.put('/edit', async (req, res) => {
    
	try{
		const bookNew = await Book.updateOne( { _id: req.body._id }, req.body);
		return res.send({ bookNew });
	} catch (err) {
		return res.status(400).send({ error: "Error update book" });
	}

});

router.delete('/delete/:id', async (req, res) => {
    
	try{
		const bookNew = await Book.deleteOne({ _id: req.params.id });
		return res.send({ bookNew });
	} catch (err) {
		return res.status(400).send({ error: "Error delete book" });
	}

});

module.exports = app => app.use('/book', router);

