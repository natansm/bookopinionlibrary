import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule }    from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ClarityModule, ClrFormsModule } from '@clr/angular';

import { AppComponent } from './app.component';
import { BookComponent } from './book/book.component';

import { BookServiceService } from './book-service/book-service.service';
import { BookFormComponent } from './book/book-form/book-form.component';

@NgModule({
  declarations: [
    AppComponent,
    BookComponent,
    BookFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ClarityModule,
    ClrFormsModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [
    BookServiceService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
