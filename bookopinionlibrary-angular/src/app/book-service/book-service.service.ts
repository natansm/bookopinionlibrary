import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpClient }  from '@angular/common/http';

import { BookModel } from '../model/book-model';

const API_ROOT: string = "https://bookopinionservice.herokuapp.com/book";

@Injectable({
  providedIn: 'root'
})
export class BookServiceService {

  constructor(private http: HttpClient) { }

  getBooks(): Observable<BookModel[]> {
     return this.http.get<BookModel[]>(API_ROOT + "/get");
  }

  getBookById(id: string): Observable<BookModel> {
     return this.http.get<BookModel>(API_ROOT + "/get/" + id);
  }

  addBook(book: BookModel): Observable<BookModel> {
    return this.http.post<BookModel>(API_ROOT + "/add", book);
  }

  updateBook(book: BookModel): Observable<BookModel> {
    return this.http.put<BookModel>(API_ROOT + "/edit", book);
  }

  deleteBook(id: string): Observable<BookModel> {
    return this.http.delete<BookModel>(API_ROOT + "/delete/" + id);
  }
}
