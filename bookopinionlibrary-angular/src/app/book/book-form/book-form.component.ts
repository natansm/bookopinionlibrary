import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { BookModel } from '../../model/book-model';

import { BookServiceService } from '../../book-service/book-service.service';

@Component({
  selector: 'app-book-form',
  templateUrl: './book-form.component.html',
  styleUrls: ['./book-form.component.css']
})
export class BookFormComponent implements OnInit {

  book: BookModel = new BookModel();

  isNotNull: boolean = false;

  constructor(private bookService: BookServiceService, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    var id = this.activatedRoute.snapshot.paramMap.get('id');
    if (id != null) {
          this.bookService.getBookById(id).subscribe(bookResult => { this.book = bookResult });
          this.isNotNull = true;
    }
  }

  save(){
    this.bookService.addBook(this.book).subscribe(_ => this.back());
  }

  update(){
    this.bookService.updateBook(this.book).subscribe(_ => this.back());
  }

  deleteBook(book: BookModel){
    this.bookService.deleteBook(book._id).subscribe(_ => this.back());
  }

  back(){
    this.router.navigate(['']);
  }
}
