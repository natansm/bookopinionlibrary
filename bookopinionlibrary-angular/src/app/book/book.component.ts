import { Component, OnInit } from '@angular/core';

import { BookModel } from '../model/book-model';

import { BookServiceService } from '../book-service/book-service.service';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {

  books: BookModel[] = [];

  constructor(private bookService: BookServiceService) { }

  ngOnInit() {
    this.bookService.getBooks().subscribe(booksResult => {
       this.books = booksResult;
    });
  }

  deleteBook(book: BookModel){
    this.bookService.deleteBook(book._id).subscribe(_ => this.ngOnInit());
  }

}
