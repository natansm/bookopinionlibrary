export class BookModel {

     public _id: string;
     public nameBook: string;
     public authorBook: string;
     public createdAt: Date;
     
}
