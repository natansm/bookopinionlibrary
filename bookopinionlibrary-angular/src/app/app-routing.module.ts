import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BookComponent } from './book/book.component';
import { BookFormComponent } from './book/book-form/book-form.component';


const routes: Routes = [
    { path: '', component: BookComponent },
    { path: 'book', component: BookFormComponent },
    { path: 'book/edit/:id', component: BookFormComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
